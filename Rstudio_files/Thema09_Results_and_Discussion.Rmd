---
title: "Thema 09 Results and Discussion"
author: "Joas Weeda"
date: "9/30/2019"
output:
  html_document:
    df_print: paged
  pdf_document:
    fig_caption: yes
---


# Results and Discussion 

## Results   
### First look at the data  
Before any good analysis can take place it's important to look at the raw data. The dataset contains 13 columns, only 10 of them contain useful numeric data. The last column contains factors on 6 levels. The other 2, num and id, are not used and can be removed from the dataset. The codebook was defined using a seperate txt-file containing the column names, titles and descriptions. Looking at the summaries of the columns there are some NA's to be found, these need to be removed before starting the machine learning process of this project.

```{r}
#Define raw data
raw_data <- data.frame(read.table("../Data_files/birds.csv", header = TRUE,
                              sep = "\t", na.strings = ""))
summary(raw_data)


#Define data to be used in plots
data <- data.frame(read.table("../Data_files/birds.csv", header = TRUE,
                              sep = "\t", na.strings = ""))[,-1:-2]
summary(data)

#Define codebook
codebook <- read.table("../Data_files/Thema09_codebook", header = TRUE,
                       sep = ";", na.strings = "")

#Set rownames codebook
rownames(codebook) <- codebook[,1]

#Define variable codebook without id-column
codebook.no.ID <- codebook[,2:3]

#Define columns with numeric data from codebook
data.columns.codebook <- codebook.no.ID[2:11,]

#Set group names (levels column "type"")
group.names <- c("Swimming Birds", "Wading Birds", "Terrestrial Birds",
                 "Raptors", "Scansorial Birds", "Singing Birds")

```
  
### Variation
A function was used to generate histograms from the data, displaying the distribution for every bone and colouring the bins using the type column. This gave a lot more inside to the data than the summary did. In the histograms of the Humerus and Tarsometatarsus bonemeasurements there are a lot of outliers. But most of the outliers are marked as swimming bird, so this must be a group of birds that have a large humerus and tarsometatarsus. That is why it did not need any adjustments.   

```{r}
library(ggplot2)
library(ggbiplot)
```

```{r}
#Histogram function used in the for-loop
bones.histogram <-function(column, histo.title, x.label, colour.column=data$type) {
  ggplot(
    data=data, 
    aes(x = column, col=colour.column)) + 
    geom_histogram(bins = 25, na.rm = T) + 
  labs(title = sprintf("Histogram distribution %s",histo.title), x=x.label, y="Count") + 
    scale_color_discrete(name = "Ecological group", 
                         labels = group.names)
}

#For-loop that generates plots
for (name in rownames(data.columns.codebook) ) {
  full.name <- codebook.no.ID[name,2]
  x.label <- data.columns.codebook[name,1]
  p <- bones.histogram(data[,name], codebook.no.ID[name,2], x.label)
  print(p)
}

```


To further explore the variation of the data, scatterplots were used. They give a better image of the distribution using a lineair model to draw a line in between the points to give a better understanding of the distribution and the spread of the data.  

```{r}
bones.scatterplot <- function(x.column, y.column, title, x.label, y.label, 
                              colour.column = data$type) {
  ggplot(
  data=data, 
  mapping=aes(x=log2(x.column), y=log2(y.column), col=colour.column)) +  
  geom_point(alpha = 1/2, na.rm = T) + 
  labs(title = sprintf("Ratio %s and Width", title), x=x.label, y=y.label, 
       color="Ecological group") + scale_color_discrete(labels = group.names) + 
    geom_smooth(method = "lm", se = FALSE, na.rm = T)
}

for (i in c(1,3,5,7,9)) {
  full.name <- data.columns.codebook[i,2]
  data1 <- data[,i]
  x.label <- data.columns.codebook[i,1]
  data2 <- data[,(i+1)]
  y.label <- data.columns.codebook[(i+1),1]
  p <- bones.scatterplot(data1, data2, full.name, x.label, y.label)
  print(p)
}

```


The scatterplots for the Femur and Tibiotarsus are very plain and don't show many variation in the lineair regression lines. The plots for the Humerus, Ulna and Tarsometatarsus show a greater variation. Because the bone measurements from these 3 are more interesting, the data for the Femur and Tibiotarsus are not used after this point.  

```{r}
bones.boxplot <-function(column, boxplot.title, y.label, colour.column=data$type) {
  ggplot(data, mapping=aes(x=colour.column, y=column)) + 
  geom_boxplot(na.rm=T) + 
  labs(title = sprintf("Boxplot %s - Type of bird", boxplot.title), x = "Type of bird", y = y.label) + 
  scale_x_discrete(labels = c("Swimming Birds", "Wading Birds", "Terrestrial Birds",
                              "Raptors", "Scansorial Birds", "Singing Birds"))
}

for (name in rownames(data.columns.codebook)[c(1:4,9,10)] ) {
  full.name <- codebook.no.ID[name,2]
  y.label <- data.columns.codebook[name,1]
  p <- bones.boxplot(data[,name], codebook.no.ID[name,2], y.label)
  print(p)
}

```

```{r, echo=FALSE, include=TRUE, fig.height=4, fig.width=8}
data.PCA <- data.frame(na.omit(data))

bones.PCAplot <- function(PCA.data, plot.title, column.group = data.PCA$type) {
  PCA.data
  g <- ggbiplot(PCA.data, obs.scale = 1, var.scale = 1,
              groups = column.group, ellipse = FALSE,
              circle = TRUE)
g <- g + scale_color_discrete(name = "Ecological group",
labels = c("Swimming Birds", "Wading Birds", "Terrestrial Birds",
           "Raptors", "Scansorial Birds", "Singing Birds"))
g <- g + theme(legend.direction = "vertical",
               legend.position = "right") + labs(title = plot.title)
g
}

ir.pca.all <- prcomp(log2(data.PCA[,c(1,2,3,4,9,10)]),
                 center = TRUE,
                 scale. = TRUE)

ir.pca.length <- prcomp(log2(data.PCA[,c(1,3,9)]),
                 center = TRUE,
                 scale. = TRUE)

ir.pca.width <- prcomp(log2(data.PCA[,c(2,4,10)]),
                 center = TRUE,
                 scale. = TRUE)


bones.PCAplot(ir.pca.all,"PCA-plot Length and Width of Femur, Humerus and Tarsometatarsus")
bones.PCAplot(ir.pca.length,"PCA-plot Length of Femur, Humerus and Tarsometatarsus")
bones.PCAplot(ir.pca.width,"PCA-plot Width of Femur, Humerus and Tarsometatarsus")

```


## Discussion  

The quality of the dataset is good, because of the many instances and small amount of NA's. Also because of the many bones that were measured. I don't think I have obtained the best possible clean version of the dataset, because I did not have a lot of experience in exploratory data analysis. I think that's also the reason why this paper is not 3-4 pages long (excluding figures and tables).
