---
title: "Thema 09 Project Machine Learning - Bird's Bones and Living Habits"
output:
  html_document:
    df_print: paged
  pdf_document: default
---

```{r, echo=FALSE, include=FALSE}
library(ggplot2)
library(ggbiplot)
```

# Data Exploration  

## Background  

The dataset Birds' Bones and Living Habits (URL can be found below this paragraph), is gonna be the dataset used in this project. The dataset contains 420 instances/birds, for every bird the following bones have been measured and documented: Humerus, Ulna, Femur, Tibiotarsus and the Tarsometatarsus. The columns of the dataset contain lenght and width for every measured bone. The last attribute type is the one that needs to be predicted using machine learning. Machine learning uses the patterns in the dataset to make these predictions.  

https://www.kaggle.com/zhangjuefei/birds-bones-and-living-habits

## September 10th

Started by looking at possible topics to use for this project, landed on an article and dataset about autism. After I checked the data I decided to resume my search, because the dataset was very discrete and did not have a lot of numeric values. Which is bad cause the numeric values usually make for better plots. I could not find a good dataset myself, so I went back to the given datasets and chose the "Birds' Bones and Living Habits" dataset because I am interested in biology and don't have a lot of knowledge on birds but also to get a bit of evidence to support the evolution theory, which I can use to go up against the creationism my parents have taught me. So I found my dataset, next thing to do is to download the dataset and get it working in R Markdown.

## September 12th

Loaded in the birds.csv file to look at the data, data looked good. Not a lot of missing values, NA's that sort of thing. So I proceeded to make a codebook, which I did not know how to do, so after a view tries I stopped to prevent me from losing a lot of time. Instead I made a temporary codebook in text in RMarkdown, I later found out that I needed a text-file that gave every column in the dataset an description and a title. This is useful when making plots, cause you can just get those from the codebook after you has read in the file. I also started to set up a few plots, cause for me this is the first time working with ggplot2.

#### Temporary codebook:

The data used in this research project contains bone measurements from birds. There are 420 birds contained in this dataset. Each bird is represented by 10 measurements.  

The attributes contained in the datafile(birds.csv) in order:  

- id; sequential id; numeric 
- huml; Length of Humerus (length (mm)); numeric
- humw; Diameter of Humerus (length (mm)); numeric
- ulnal; Length of Ulna (length (mm)); numeric
- ulnaw; Diameter of Ulna (length (mm)); numeric
- feml; Length of Femur (length (mm)); numeric
- femw; Diameter of Femur (length (mm)); numeric
- tibl; Length of Tibiotarsus (length (mm)); numeric
- tibw; Diameter of Tibiotarsus (length (mm)); numeric
- tarl; Length of Tarsometatarsus (length (mm)); numeric
- tarw; Diameter of Tarsometatarsus (length (mm)); numeric
- type; Ecological Group. (factor, levels: SW, W, T, R, P and SO); factor

All of the attributes, except the column type, have the datatype numeric. The column 'type' has the datatype factor with 6 levels: SW; Swimming Birds, W; Wading Birds, T; Terrestrial Birds, R; Raptors, P; Scansorial Birds, SO; Singing Birds. 

# Exploratory Data Analysis

## Read data & Codebook
  
```{r, include=TRUE, echo=FALSE}
#Filenames for the data- and codebookfile
datafile <- "../Data_files/birds.csv"
codebookfile <- "../Data_files/Thema09_codebook"

#Read data- and codebookfile
raw_data <- data.frame(read.table(datafile, header = TRUE,
                              sep = "\t", na.strings = ""))
  data <- data.frame(read.table(datafile, header = TRUE,
                              sep = "\t", na.strings = ""))[,-1:-2]
clean_data <- na.omit(data.frame(read.table(datafile, header = TRUE,
                              sep = "\t", na.strings = ""))[,c(3:13)])
codebook <- read.table(codebookfile, header = TRUE,
                       sep = ";", na.strings = "")

#Set rownames codebook
rownames(codebook) <- codebook[,1]

#Define variable codebook without id-column
codebook.no.ID <- codebook[,2:3]

#Define columns with numeric data from codebook
data.columns.codebook <- codebook.no.ID[2:11,]

#Set group names (levels column "type"")
group.names <- c("Swimming Birds", "Wading Birds", "Terrestrial Birds",
                 "Raptors", "Scansorial Birds", "Singing Birds")

#Show a summary of the data
summary(clean_data)
```

## September 17th

Worked on making plots for individual columns in the dataset. Started by generating a scatterplot which had the Femur width and length on y- and x-axis, at first this plot was not very useful. But after I started coloring the points according to the ecological groups and log2 transformated the data, the plot made a lot more sense. You could see clusters of points with the same ecological group. The same happened for the histogram I generated, I still need to add color to the bins. Also made a boxplot, showing the data distribution for every group. I was still trying to figure out how to use ggplot2 to make the plots display things that were useful, so it was a lot of "Google is your friend" and "Stackoverflow", but I feel like I'm getting better.

```{r, include=TRUE, echo=FALSE}
bones.scatterplot <- function(x.column, y.column, title, x.label, y.label, 
                              colour.column = data$type) {
  ggplot(
  data=data, 
  mapping=aes(x=log2(x.column), y=log2(y.column), col=colour.column)) +  
  geom_point(alpha = 1/2, na.rm = T) + 
  labs(title = sprintf("Ratio %s and Width", title), x=x.label, y=y.label, 
       color="Ecological group") + scale_color_discrete(labels = group.names) + 
    geom_smooth(method = "lm", se = FALSE, na.rm = T)
}

for (i in c(1,3,5,7,9)) {
  full.name <- data.columns.codebook[i,2]
  data1 <- data[,i]
  x.label <- data.columns.codebook[i,1]
  data2 <- data[,(i+1)]
  y.label <- data.columns.codebook[(i+1),1]
  p <- bones.scatterplot(data1, data2, full.name, x.label, y.label)
  print(p)
}

```

```{r, include=TRUE, echo=FALSE}
bones.histogram <-function(column, histo.title, x.label, colour.column=data$type) {
  ggplot(
    data=data, 
    aes(x = column, col=colour.column)) + 
    geom_histogram(bins = 25, na.rm = T) + 
  labs(title = sprintf("Histogram distribution %s",histo.title), x=x.label, y="Count") + 
    scale_color_discrete(name = "Ecological group", 
                         labels = group.names)
}

# bones.barplot <-function(column, histo.title, x.label, colour.column=data$type) {
#   ggplot(
#     data=data,
#     aes(x = column, col=colour.column)) +
#     geom_col(na.rm = T) +
#   labs(title = sprintf("Histogram distribution %s",histo.title), x=x.label, y="Count") +
#     scale_color_discrete(name = "Ecological group",
#                          labels = group.names)
# }

for (name in rownames(data.columns.codebook)[c(1:4,9,10)] ) {
  full.name <- codebook.no.ID[name,2]
  x.label <- data.columns.codebook[name,1]
  p <- bones.histogram(data[,name], codebook.no.ID[name,2], x.label)
  print(p)
}

# for (name in rownames(data.columns.codebook)[c(1:4,9,10)] ) {
#   full.name <- codebook.no.ID[name,2]
#   x.label <- data.columns.codebook[name,1]
#   p <- bones.barplot(data[,name], codebook.no.ID[name,2], x.label)
#   print(p)
# }


```

```{r, include=TRUE, echo=FALSE}
bones.boxplot <-function(column, boxplot.title, y.label, colour.column=data$type) {
  ggplot(data, mapping=aes(x=colour.column, y=column)) + 
  geom_boxplot(na.rm=T) + 
  labs(title = sprintf("Boxplot %s - Type of bird", boxplot.title), x = "Type of bird", y = y.label) + 
  scale_x_discrete(labels = c("Swimming Birds", "Wading Birds", "Terrestrial Birds",
                              "Raptors", "Scansorial Birds", "Singing Birds"))
}

for (name in rownames(data.columns.codebook)[c(1:4,9,10)] ) {
  full.name <- codebook.no.ID[name,2]
  y.label <- data.columns.codebook[name,1]
  p <- bones.boxplot(data[,name], codebook.no.ID[name,2], y.label)
  print(p)
}

```

## September 24th

The code from last week got replaced by the functions and for loops I now use to generate multiple plots at a time. The functions contain the code I made last week to generate the plots, but with placeholders for the arguments. Using for-loops the program generates plots for every column in the dataset. It uses the rownames of the codebook to get the correct data and also a title and label for the axis. I gave the histogram some more spice by coloring the sections of the bins and made a function and for-loop to get a histogram for every column. Also started working on a PCA-plot, which displays how the different bones are grouped together and gave every group a color using the types from the codebook.


```{r, include=TRUE, echo=FALSE, fig.height=8, fig.width=8}
data.PCA <- data.frame(na.omit(data))

bones.PCAplot <- function(PCA.data, plot.title, column.group = data.PCA$type) {
  PCA.data
  g <- ggbiplot(PCA.data, obs.scale = 1, var.scale = 1,
              groups = column.group, ellipse = FALSE,
              circle = TRUE)
g <- g + scale_color_discrete(name = "Ecological group",
labels = c("Swimming Birds", "Wading Birds", "Terrestrial Birds",
           "Raptors", "Scansorial Birds", "Singing Birds"))
g <- g + theme(legend.direction = "vertical",
               legend.position = "right") + labs(title = plot.title)
g
}

ir.pca.all <- prcomp(log2(data.PCA[,c(1:10)]),
                 center = TRUE,
                 scale. = TRUE)

ir.pca.length <- prcomp(log2(data.PCA[,c(1,3,9)]),
                 center = TRUE,
                 scale. = TRUE)

ir.pca.width <- prcomp(log2(data.PCA[,c(2,4,10)]),
                 center = TRUE,
                 scale. = TRUE)


bones.PCAplot(ir.pca.all,"PCA-plot Length and Width of Femur, Humerus and Tarsometatarsus")
bones.PCAplot(ir.pca.length,"PCA-plot Length of Femur, Humerus and Tarsometatarsus")
bones.PCAplot(ir.pca.width,"PCA-plot Width of Femur, Humerus and Tarsometatarsus")

```

## September 26th
